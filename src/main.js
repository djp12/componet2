import Vue from 'vue'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false


// 全局组件和全局过滤器。如果你在每一个页面当中都有用到该组件，建议写在全局。如果达不到80%以上的页面使用，
// 我建议放在各自的页面中。
// 因为放在全局会导致页面加载速度变慢
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
